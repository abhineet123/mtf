var searchData=
[
  ['nccparams',['NCCParams',['../structNCCParams.html#a792823877cfb2c7c3e2cc8b29e19a0a7',1,'NCCParams::NCCParams(const AMParams *am_params, bool _fast_hess, double _likelihood_alpha)'],['../structNCCParams.html#a42ac2c56df37193dbdd567eab5ad1f06',1,'NCCParams::NCCParams(const NCCParams *params=nullptr)']]],
  ['nssdparams',['NSSDParams',['../structNSSDParams.html#acd5af96c8a4656b49124db3c65307c59',1,'NSSDParams::NSSDParams(const AMParams *am_params, double _norm_pix_max, double _norm_pix_min, bool _debug_mode)'],['../structNSSDParams.html#a5ac90a50027d4d8ba7fb1c1757e46444',1,'NSSDParams::NSSDParams(const NSSDParams *params=nullptr)']]]
];
